# XNAT Data Scraper

The XNAT data scraper pulls JSON, XML, and binary data for experiments on an XNAT server. This can be used for templatizing sample data for load testing.

## Building

The scraper is a Groovy script and class. The build is really just a jar that contains these Groovy files without compiling them to Java class files. To build the jar file from the repository folder, type:

`mvn clean package`

## Example Invokation

All of the dependencies for the data scraper are referenced dynamically using Groovy's **@Grab** annotation, so there's no need to include other dependencies. Run the script with Groovy:

`groovy -classpath scraper-1.0-SNAPSHOT.jar jar:file:scraper-1.0-SNAPSHOT.jar'!'/scrape.groovy -a https://cnda-dev-cjm.nrg.mir -p rixprj01 -s rixprj01_subj01 -d ~/work -b true -z -u username:password`

You can change the host, project, and subject as desired. It's probably best for the username and password if you use a valid alias token and secret.

## Contributors

Rick Herrick wrote this thing.

## License

The XNAT data scraper is released under the terms of the [BSD 2-clause license](http://opensource.org/licenses/BSD-2-Clause).

