// -a https://cnda-dev-cjm.nrg.mir -p DIAN_010 -s 0085741 -u 367caf60-1c1d-4c5d-ab1d-12d53dfbe8dd:1404160160958

package org.nrg.tools

@Grab(group = 'org.apache.commons', module = 'commons-lang3', version = '3.3.2')
@Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.2')
@Grab(group = 'org.slf4j', module = 'slf4j-api', version = '1.7.7')
@Grab(group = 'org.slf4j', module = 'slf4j-log4j12', version = '1.7.7')
@Grab(group='log4j', module='log4j', version='1.2.17')

import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.protocol.HttpContext

import java.nio.file.NotDirectoryException

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.TEXT

@Slf4j
class Scraper {
    String archivePath
    final URI uri
    final String projectId, subject, username, password
    final RESTClient http
    String destination
    File folder
    boolean showJson
    boolean showXml
    String output
    boolean zip
    boolean includeBinaries

    public Scraper(final URI uri, final String projectId, final String subject, final String username, final String password) {
        this.uri = uri
        this.projectId = projectId
        this.subject = subject
        this.username = username
        this.password = password
        http = new RESTClient(uri)
        http.ignoreSSLIssues()
        (http.client as DefaultHttpClient).addRequestInterceptor(new HttpRequestInterceptor() {
            void process(HttpRequest httpRequest, HttpContext httpContext) {
                httpRequest.addHeader 'Authorization', 'Basic ' + "${username}:${password}".bytes.encodeBase64().toString()
            }
        })

        HttpResponseDecorator response = http.get(path: "/data/services/settings/archivePath", contentType: JSON, headers: [Accept: "*/*"]) as HttpResponseDecorator

        if (!response.success) {
            throw new RuntimeException("Check of server head returned error status: [${response.status}] ${response.statusLine}")
        }

        archivePath = response.data.ResultSet.Result

        log.info "Got return status ${response.status} in response to GET request to URI ${uri}, found archive path at ${archivePath}"
    }

    public def run() {
        if (destination) {
            folder = new File(destination)
            if (!folder.exists()) {
                throw new FileNotFoundException("Couldn't find the folder ${destination}")
            }
            if (!folder.isDirectory()) {
                throw new NotDirectoryException("The specified destination ${destination} is not a directory.")
            }
        } else {
            folder = new File(".")
        }

        HttpResponseDecorator response = http.get(path: "/data/projects/${projectId}/subjects/${subject}/experiments", query: [format: "json"]) as HttpResponseDecorator
        if (!response.success) {
            println "Got a non-success return code in response to the project ${projectId} and subject ${subject}: ${response.statusLine}"
            return
        }

        if (response.data.ResultSet == null || response.data.ResultSet.Result == null) {
            println "There were no results found in the server response."
            return
        }

        log.info "Got a response with content type ${response.contentType} and ${response.data.ResultSet.Result.size}"

        // TODO: I have to assign projectId to this id so that I can use it inside this closure. For some reason,
        // TODO: building with projectId inside there causes an error:
        // TODO: Error:Groovyc: BUG! exception in phase 'class generation' in source unit '/Users/rherrick/Development/XNAT/1.6/datascraper/src/main/java/org/nrg/tools/Scraper.groovy'
        // TODO: tried to get a variable with the name projectId as stack variable, but a variable with this name was not created
        final def id = projectId
        response.data.ResultSet.Result.each { final Map result ->
            final String json = getResourceByURIAndFormat(result, "json")
            final String label = new JsonSlurper().parseText(json).items.data_fields.label.get(0)
            if (includeBinaries) {
                File archive = new File("${archivePath}/${id}/arc001/${label}")
                if (archive.exists()) {
                    new AntBuilder().copy( todir: getOutputFolder(label) ) {
                        fileset( dir: "${archivePath}/${id}/arc001/${label}" )
                    }
                }
            }

            if (showXml) {
                getResourceByURIAndFormat(result, "xml")
            }

            if (zip) {
                AntBuilder ant = new AntBuilder()
                ant.fileset(id: "files", dir: folder) {
                    include: '**/*'
                }

                def file = new File(folder, "${label}.zip")
                ant.zip(destfile: file, duplicate: "preserve") {
                    fileset(refid: "files")
                }
                if (file.exists()) {
                    new File("${folder}/${label}").deleteDir()
                }
            }
        }
    }

    // TODO: Add a closure parameter to pass in pretty-print code for JSON and XML.
    private def getResourceByURIAndFormat(final Map resource, final String extension) {
        final StringWriter buffer = new StringWriter();

        http.get(path: resource.URI, query: [format: extension], contentType: TEXT, headers: [Accept: "*/*"]) { experiment, data ->
            if (!experiment.success) {
                log.info("An error occurred downloading the experiment ${resource.label}: {$experiment.status}")
            } else {
                log.info "Got a response for experiment ${resource.label} with content type ${experiment.contentType}"
                getOutputFile(resource.label as String, extension).withWriter { writer ->
                    // TODO: In here you'd invoke the pretty-print and use that to format the output.
                    data.eachLine { line ->
                        writer.write("${line}\n" as String)
                        buffer.write("${line}\n" as String)
                    }
                }
            }
        }
        buffer.toString()
    }

    private File getOutputFolder(String label) {
        def destination = new File(folder, label)
        if (!destination.exists()) {
            boolean success = destination.mkdirs()
            if (!success) {
                throw new FileNotFoundException("Something went wrong trying to create the output folder ${destination.absolutePath}")
            }
        }
        destination
    }

    private File getOutputFile(final String label, final String extension) {
        new File(getOutputFolder(label), "${label}.${extension}")
    }
}
