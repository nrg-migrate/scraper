import org.nrg.tools.Scraper

def cli = new CliBuilder(usage: 'scrape [options]', header: 'Options: (options marked with * are required)')
cli.a(args: 1, argName: 'url', '* Base address for the target XNAT server')
cli.b(args: 1, argName: 'true/false', 'Indicates whether binaries should be copied if accessible. Defaults to true.')
cli.d(args: 1, argName: 'path', 'Destination folder for all retrieved documents')
cli.h(args: 0, 'Display help and usage information for the scrape utility')
cli.j(args: 0, 'Limit retrieve to JSON documents (default is to retrieve both XML and JSON)')
cli.l(args: 1, argName: 'path', 'Log operations to the indicated log file')
cli.p(args: 1, argName: 'project', '* ID for the target project')
cli.s(args: 1, argName: 'subject', '* ID for the target subject')
cli.u(args: 1, argName: 'user:pass', '* Username and password for the target XNAT server, separated by the : character.')
cli.x(args: 0, 'Limit retrieve to XML documents (default is to retrieve both XML and JSON)')
cli.z(args: 0, 'Create zip archive of each retrieved experiment')

def options = cli.parse(args)
if (options.h) {
    cli.usage()
    return
}
def errors = []

URI uri = null
String projectId = null, subject = null, username = null, password = null

if (!options.a) {
    errors.add 'You must specify the address for the target XNAT server with the -a option.'
} else {
    try {
        uri = new URI(options.a)
    } catch (URISyntaxException ignored) {
        errors.add "The URI for the target XNAT server has an invalid URI format: ${options.a}"
    }
}
if (options.b) {
    if (!options.b.equals("true") && !options.b.equals("false")) {
        errors.add 'You must specify either true or false for the -b option.'
    }
}

if (!options.p) {
    errors.add 'You must specify the project ID for the scraping operation with the -p option.'
} else {
    projectId = options.p
}
if (!options.s) {
    errors.add 'You must specify the subject ID for the scraping operation with the -s option.'
} else {
    subject = options.s
}
if (!options.u) {
    errors.add 'You must specify the username and password for the target XNAT server with the -u option in the form username:password.'
} else {
    if (options.u ==~ ~/[A-z0-9_-]+:.+/) {
        def atoms = options.u.split(':')
        username = atoms[0]
        password = atoms[1]
    } else {
        errors.add 'You must specify the username and password for the target XNAT server with the -u option in the form username:password.'
    }
}
if (errors.size() > 0) {
    println "Found errors in your command syntax:\n"
    errors.each {
        println " * ${it}"
    }
    println()
    cli.usage()
    return
}
assert uri != null
assert projectId != null
assert subject != null
assert username != null
assert password != null
println "Launching scrape operation with the following options:"
println " URL:       ${uri}"
println " Project:   ${projectId}"
println " Subject:   ${subject}"
println " Username:  ${username}"
println " Password:  ${password}"

Scraper scraper = new Scraper(uri, projectId, subject, username, password)
cli.z(args: 1, argName: 'fileName', 'Create zip archive of all retrieved documents')
if (options.b) {
    scraper.includeBinaries = Boolean.parseBoolean(options.b)
}
if (options.d) {
    scraper.destination = options.d
}
if (options.j == options.x) {
    scraper.showJson = scraper.showXml = true
} else if (options.j) {
    scraper.showJson = true
} else {
    scraper.showXml = true
}
if (options.l) {
    scraper.output = options.l
    println "The -l logging option is not yet implemented."
}
if (options.z) {
    scraper.zip = true
}
scraper.run()